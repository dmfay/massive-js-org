---
layout: Documentation
title: Upgrade Notes
---

This page documents things you should bear in mind when upgrading Massive. The [changelog](changelog) is also worth checking out.

<!-- vim-markdown-toc GFM -->

* [v6.0.0](#v600)
  * [Node Versions](#node-versions)
  * [Decomposition Behavior](#decomposition-behavior)
  * [Implicit Ordering](#implicit-ordering)
  * [The Type Option](#the-type-option)

<!-- vim-markdown-toc -->

## v6.0.0
### Node Versions

Pre-ES2017 versions of Node (< 7.6) are no longer supported.

### Decomposition Behavior

Resultset decomposition should have defaulted to yielding arrays all along: the natural habitat of a record is in a set of other records. Thanks to the opportunity afforded by a major version bump, I'm correcting this oversight.

Resultset decomposition now yields arrays by default, and the `array` option has therefore been removed. **You will need to remove `array: true` where it occurs in your decomposition schemas when upgrading**.

To yield a flat object (as the former default) instead, set `decomposeTo: 'object'`.

### Implicit Ordering

In previous versions, Massive would automatically order results by the primary key of the relation being queried, or in other circumstances the first column (using positional arguments). This is no longer done; results are returned in the order Postgres reads them, unless `options.order` has been [explicitly specified](options-objects#ordering-results).

You may find you have been depending on this implicit ordering behavior, especially in tests.

### The Type Option
Improvements to the parser have rendered the `type` field in the `order` option superfluous. Use Postgres-style casting in the `field` instead:

```js
{
  order: [{field: 'myfield::int', direction: 'desc'}]
}
```

`type` continues to function for now, but can be considered deprecated and subject to removal in a future major release.
