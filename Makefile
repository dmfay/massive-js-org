build: clean
	mkdir -p pages/api
	mkdir -p public/img
	mkdir -p public/css
	mkdir -p public/fonts

	echo -e "---\nlayout: Documentation\n---\n" > pages/docs/changelog.md
	sed 's/^#/##/' node_modules/massive/CHANGELOG.md >> pages/docs/changelog.md
	npx babel-node lib/api.js
	npx babel-node lib
	npx lessc styles/site.less public/css/site.css

	cp img/* public/img
	cp fonts/* public/fonts
	cp .htaccess public/

clean:
	rm -rf pages/api
	rm -f pages/docs/changelog.md
	rm -rf public

lint:
	eslint '.'
