import Site from './Site';

export default function ({typo, title, content}) {
	const h2 = title ? (<h2>{title}</h2>) : null;

  return (
    <article>
			{h2}
      <div>
        {typo(content)}
      </div>
    </article>
  );
};
