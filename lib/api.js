'use strict';

const jsdoc2md = require('jsdoc-to-markdown');
const fs = require('fs');
const path = require('path');

/* input and output paths */
const outputDir = __dirname + '/../pages/api';
console.log(outputDir)

console.log('generating api docs');

const templateData = jsdoc2md.getTemplateDataSync({ files: ['node_modules/massive/index.js', 'node_modules/massive/lib/*'] });

/* reduce templateData to an array of class names */
const classNames = templateData.reduce((classNames, identifier) => {
  if (identifier.kind === 'class') {
    classNames.push(identifier.name);
  }

  return classNames;
}, []);

/* create a documentation file for each class */
for (const className of classNames) {
  const template = `{{#class name="${className}"}}{{>docs}}{{/class}}`;
  console.log(`rendering ${className}, template: ${template}`);
  const output = jsdoc2md.renderSync({
    data: templateData,
    template: template,
    partial: [
			'jsdoc/header.hbs'
		],
    helper: ['jsdoc/helpers.js']
  });
  fs.writeFileSync(path.resolve(outputDir, `${className}.md`), `---\nlayout: Documentation\ntitle: ${className}\nnoHeading: true\n---\n\n${output}`);
}
