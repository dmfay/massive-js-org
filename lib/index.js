import {
  start,
  loadConfig,
  loadSourceFiles,
  generatePages,
  savePages,
  createMarkdownRenderer,
  createTemplateRenderer,
  helpers
} from 'fledermaus';
import moment from 'moment';
import slug from 'remark-slug';
import visit from 'unist-util-visit';

const indexer = (page, idx) => {
  page.index = idx;

  return page;
};

start('Building site...');

const config = loadConfig('config');
const options = config.base;

const toBuild = loadSourceFiles(options.staticFolder, options.staticExts, {
  renderers: {
    md: createMarkdownRenderer({
      plugins: [
        slug,
        () => tree => {
          visit(tree, 'heading', node => {
            visit(node, 'text', textNode => {
              // add a space between function names and signatures in headings
              // so text can break between them
              textNode.value = textNode.value.replace(/([^\s])\(/, '$1 (');
            });
          });
        }
      ]
    })
  }
});

const pages = generatePages(toBuild, config, helpers, {
  jsx: createTemplateRenderer({
    root: options.templatesFolder
  })
});

savePages(pages, options.publicFolder);
