# MassiveJS.org

Contains the documentation for [MassiveJS](https://massivejs.org).

## Contributing

If you have an improvement in mind for the docs, feel free to suggest it in an issue or send a merge request.

One note: the current stylesheet has a rather low usable width for documentation pages. Code examples with lines longer than around 55 characters break the layout, scroll, or both. Please try to stay at or under that limit if you can!
